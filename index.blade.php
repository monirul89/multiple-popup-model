@extends('public.layout')

@section('title')
    THE ISHO HOTEL - A POP UP
@endsection

@push('meta')
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
@endpush

@push('styles')
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/reset.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/wordpress.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/style.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/modulobox.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/font-awesome.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/themify-icons.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/tooltipster.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/demo.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/loftloader/assets/css/loftloader.min.css?ver=2020081301" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/lib/animations/animations.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/css/frontend-legacy.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/css/frontend.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/css/swiper.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/css/animatedheadline.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/css/justifiedGallery.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/css/flickity.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/css/owl.theme.default.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/css/hoteller-elementor.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/css/hoteller-elementor-responsive.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0" type="text/css" media="all" />
        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/responsive.css" type="text/css" media="all" />

        <link rel="stylesheet" href="https://isho.com/themes/storefront/isho/popup-hotel/css/custom-css.css" type="text/css" media="all" />
        <link rel="stylesheet" href="{{ url('themes/storefront/isho/hotel/custom-style.css') }}" type="text/css" media="all" />

	    <link rel="icon" type="image/png" sizes="96x96" href="https://isho.com/favicon-96x96.png">
@endpush

@section('body_class', 'home page-template-default page page-id-187 woocommerce-no-js tg_menu_transparent tg_lightbox_black tg_sidemenu_desktop leftalign loftloader-lite-enabled elementor-default elementor-kit-1577 elementor-page elementor-page-187')


@section('content')
        <div id="perspective">
                <!-- Begin content -->
                <!-- <section class="popup-video">
                    <div class="pop-video">
                        <video style="width:100%" loop autoplay muted controls id="vid">
                                <source type="video/mp4" src="https://www.isho.com/storage/video/pop-up.mp4"></source>
                                <source type="video/ogg" src="https://www.isho.com/storage/video/pop-up.mp4"></source>
                        </video>
                        <div class="elementor-widget-container">
                            <div class="slide-content">
                                <div class="caption">
                                    <div class="title">
                                        <h2> ISHO HOTEL <br> <span>A POP UP</span> </h2>
                                    </div>
                                    <div class="text">
                                        EXPERIENCE THE BEST OF LUXURY AND LIFESTYLE
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section> -->
                <div id="page_content_wrapper" class="noheader">
                    <div class="inner">
                        <!-- Begin main content -->
                        <div class="inner_wrapper">
                            <div class="sidebar_content full_width">
                                <div data-elementor-type="wp-post" data-elementor-id="187" class="elementor elementor-187" data-elementor-settings="[]">
                                    <div class="elementor-inner">
                                        <div class="elementor-section-wrap">
                                            <!-- start Video -->
                                            <!-- <section
                                                class="popup-video elementor-section elementor-top-section elementor-element elementor-element-e194771 elementor-section-stretched elementor-section-height-min-height elementor-section-full_width elementor-section-height-default elementor-section-items-middle"
                                                data-id="e194771"
                                                data-element_type="section"
                                                data-settings='{"stretch_section":"section-stretched","background_background":"video","background_video_link":"https://www.youtube.com/watch?v=hUpU-FWvqCg&amp;rel=0","background_video_start":2,"background_video_end":120,"hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}'
                                                style="height: 800px;">
                                                <div class="elementor-background-video-container elementor-hidden-phone"><div class="elementor-background-video-embed"></div></div>
                                                <div class="elementor-background-overlay"></div>
                                                <div class="elementor-container elementor-column-gap-default pop-video">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2c4642d"
                                                            data-id="2c4642d"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                        >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-8ea9f25 elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="8ea9f25"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default"
                                                                    >
                                                                        <div class="elementor-widget-container">
                                                                            <div class="slide-content">
                                                                                <div class="caption">
                                                                                    <div class="title">
                                                                                        <h2> ISHO HOTEL <br> <span>A POP UP</span> </h2>
                                                                                    </div>
                                                                                    <div class="text">
                                                                                        EXPERIENCE THE BEST OF LUXURY AND LIFESTYLE
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section> -->
                                            <!-- start Video -->

                                          <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-e27aa87 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                                                data-id="e27aa87"
                                                data-element_type="section"
                                                data-settings='{"stretch_section":"section-stretched","hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}'>
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-305e677"
                                                            data-id="305e677"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}' >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-8f1346b animated-slow elementor-invisible elementor-widget elementor-widget-hoteller-slider-parallax"
                                                                        data-id="8f1346b"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeIn","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="hoteller-slider-parallax.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="slider_parallax_wrapper" data-autoplay="6000" data-pagination="0" data-navigation="0">
                                                                                <div class="slider_parallax_inner">
                                                                                    <div class="slider_parallax_slides">
                                                                                        <div class="slide is-active">
                                                                                            <div class="slide-content">
                                                                                                <div class="caption">
                                                                                                    <div class="title">
                                                                                                        <h2> ISHO HOTEL <br> <span>A POP UP</span> </h2>
                                                                                                    </div>
                                                                                                    <div class="text">
                                                                                                    EXPERIENCE THE BEST OF LUXURY AND LIFESTYLE
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="image-container">
                                                                                                <img src="https://isho.com/storage/media/pop-up/slider1.jpg" alt="" class="image" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="slide">
                                                                                            <div class="slide-content">
                                                                                                <div class="caption">
                                                                                                    <div class="title"><h2> ISHO HOTEL <br><span>A POP UP</span> </h2></div>
                                                                                                    <div class="text">
                                                                                                        Enjoy your staycation in the lap of nature
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="image-container">
                                                                                                <img src="https://isho.com/storage/media/pop-up/slider2.jpg" alt="" class="image" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="slide">
                                                                                            <div class="slide-content">
                                                                                                <div class="caption">
                                                                                                    <div class="title"><h2> ISHO HOTEL <br> <span>A POP UP</span> </h2></div>
                                                                                                    <div class="text">
                                                                                                        MAGICAL VIEWS TO LAST A LIFETIME
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="image-container">
                                                                                                <img src="https://isho.com/storage/media/pop-up/slider3.jpg" alt="" class="image" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>

                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-b10bbac elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="b10bbac"
                                                data-element_type="section"
                                                data-settings='{"hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}' >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f92e7d3"
                                                            data-id="f92e7d3"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'>
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-1a06f2b elementor-widget elementor-widget-hoteller-availability-search"
                                                                        data-id="1a06f2b"
                                                                        data-element_type="widget"
                                                                        data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="hoteller-availability-search.default" >
                                                                        <div class="elementor-widget-container">
                                                                            <div class="availability_search_wrapper">
                                                                                <div class="mphb_sc_search-wrapper">
                                                                                    <form method="GET" class="mphb_sc_search-form" action="https://themes.themegoods.com/hoteller/sites/search-results/" style="text-align: center;">
                                                                                        <p class="mphb-required-fields-tip">
                                                                                            <small> Required fields are followed by <abbr title="required">*</abbr> </small>
                                                                                        </p>
                                                                                        <p class="mphb_sc_search-check-in-date">
                                                                                            <label for="mphb_check_in_date-mphb-search-form-6061a36060295"> Check-in <abbr title="Formatted as dd/mm/yyyy">*</abbr> </label>
                                                                                            <br />
                                                                                            <input
                                                                                                id="mphb_check_in_date-mphb-search-form-6061a36060295"
                                                                                                data-datepick-group="mphb-search-form-6061a36060295"
                                                                                                value=""
                                                                                                placeholder="Check-in Date"
                                                                                                required="required"
                                                                                                type="date"
                                                                                                name="mphb_check_in_date"
                                                                                                class="mphb-datepick"
                                                                                                autocomplete="off"/>
                                                                                        </p>

                                                                                        <p class="mphb_sc_search-check-out-date">
                                                                                            <label for="mphb_check_out_date-mphb-search-form-6061a36060295"> Check-out <abbr title="Formatted as dd/mm/yyyy">*</abbr> </label>
                                                                                            <br />
                                                                                            <input
                                                                                                id="mphb_check_in_date-mphb-search-form-61ac59c4c0789"
                                                                                                data-datepick-group="mphb-search-form-6061a36060295"
                                                                                                value=""
                                                                                                placeholder="Check-out Date"
                                                                                                required="required"
                                                                                                type="date"
                                                                                                name="mphb_check_out_date"
                                                                                                class="mphb-datepick"
                                                                                                autocomplete="off"/>
                                                                                        </p>

                                                                                        <!-- <p class="mphb_sc_search-adults">
                                                                                            <label for="mphb_adults-mphb-search-form-6061a36060295"> Adults </label>
                                                                                            <br />
                                                                                            <select id="mphb_adults-mphb-search-form-6061a36060295" name="mphb_adults">
                                                                                                <option value="1" selected="selected">1</option>
                                                                                                <option value="2">2</option>
                                                                                                <option value="3">3</option>
                                                                                                <option value="4">4</option>
                                                                                                <option value="5">5</option>
                                                                                                <option value="6">6</option>
                                                                                                <option value="7">7</option>
                                                                                                <option value="8">8</option>
                                                                                                <option value="9">9</option>
                                                                                                <option value="10">10</option>
                                                                                                <option value="11">11</option>
                                                                                                <option value="12">12</option>
                                                                                                <option value="13">13</option>
                                                                                                <option value="14">14</option>
                                                                                                <option value="15">15</option>
                                                                                                <option value="16">16</option>
                                                                                                <option value="17">17</option>
                                                                                                <option value="18">18</option>
                                                                                                <option value="19">19</option>
                                                                                                <option value="20">20</option>
                                                                                                <option value="21">21</option>
                                                                                                <option value="22">22</option>
                                                                                                <option value="23">23</option>
                                                                                                <option value="24">24</option>
                                                                                                <option value="25">25</option>
                                                                                                <option value="26">26</option>
                                                                                                <option value="27">27</option>
                                                                                                <option value="28">28</option>
                                                                                                <option value="29">29</option>
                                                                                                <option value="30">30</option>
                                                                                            </select>
                                                                                        </p>

                                                                                        <p class="mphb_sc_search-children">
                                                                                            <label for="mphb_children-mphb-search-form-6061a36060295"> Children </label>
                                                                                            <br />
                                                                                            <select id="mphb_children-mphb-search-form-6061a36060295" name="mphb_children">
                                                                                                <option value="0" selected="selected">0</option>
                                                                                                <option value="1">1</option>
                                                                                                <option value="2">2</option>
                                                                                                <option value="3">3</option>
                                                                                                <option value="4">4</option>
                                                                                                <option value="5">5</option>
                                                                                                <option value="6">6</option>
                                                                                                <option value="7">7</option>
                                                                                                <option value="8">8</option>
                                                                                                <option value="9">9</option>
                                                                                                <option value="10">10</option>
                                                                                            </select>
                                                                                        </p> -->

                                                                                        <input id="mphb_check_in_date-mphb-search-form-6061a36060295-hidden" value="" type="hidden" name="mphb_check_in_date" />
                                                                                        <input id="mphb_check_out_date-mphb-search-form-6061a36060295-hidden" value="" type="hidden" name="mphb_check_out_date" />
                                                                                        <p class="mphb_sc_search-submit-button-wrapper">
                                                                                            <input type="submit" value="Search" />
                                                                                        </p>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-f194cea elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="f194cea"
                                                data-element_type="section"
                                                data-settings='{"hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}'>
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6056877"
                                                            data-id="6056877"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'>
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-006eb92 elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="006eb92"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default">
                                                                        {{--<div class="elementor-widget-container">
                                                                            <span class="elementor-heading-title elementor-size-default">Urban & Unique</span>
                                                                        </div>--}}
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-c5b8fc1 elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="c5b8fc1"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":200,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default" >
                                                                        <div class="elementor-widget-container story-title">
                                                                            <h2 class="elementor-heading-title elementor-size-default">
                                                                            Our Story
                                                                            </h2>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-9916709 elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="9916709"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":400,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default" >
                                                                        <!-- <div class="elementor-widget-container">
                                                                            <span class="elementor-heading-title elementor-size-default">Our accommodations are excellent for a trip with friends, family or as a couple</span>
                                                                        </div> -->
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-bb8a693 elementor-invisible elementor-widget elementor-widget-text-editor"
                                                                        data-id="bb8a693"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":600,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="text-editor.default" >
                                                                        <div class="elementor-widget-container elementor-para">
                                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                                <p class="p1"> We embarked on a journey to create an immersive local experience that’s one of a kind. Where people can engage with the culture and communities of the region, in the midst of untouched natural beauty.</p>
                                                                                    <p class="p1" style="padding-top: 0;">Creating a luxurious escape on the shores of the beguiling Inani beach, we’ve built a serene getaway for you. We bring to you, the best of local flavours, crafts, traditions and activities for a memorable sojourn.</p>


                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-512bfa1 elementor-invisible elementor-widget elementor-widget-image"
                                                                        data-id="512bfa1"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":800,"hoteller_image_is_animation":"false","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="image.default" >
                                                                        {{--<div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <img
                                                                                    width="432"
                                                                                    height="124"
                                                                                    src="https://isho.com/themes/storefront/isho/popup-hotel/upload/signature.png"
                                                                                    class="attachment-full size-full"
                                                                                    alt=""
                                                                                    loading="lazy"
                                                                                />
                                                                            </div>
                                                                        </div>--}}
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-33ec55d elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="33ec55d"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":1000,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default" >
                                                                        {{--<div class="elementor-widget-container">
                                                                            <span class="elementor-heading-title elementor-size-default">Hotel Manager</span>
                                                                        </div>--}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-85c4d96 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                                                data-id="85c4d96"
                                                data-element_type="section"
                                                data-settings='{"stretch_section":"section-stretched","hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}' >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-157269f"
                                                            data-id="157269f"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}' >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-3dffcd5 elementor-widget elementor-widget-hoteller-mouse-driven-vertical-carousel"
                                                                        data-id="3dffcd5"
                                                                        data-element_type="widget"
                                                                        data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="hoteller-mouse-driven-vertical-carousel.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="tg_mouse_driven_vertical_carousel_wrapper">
                                                                                <header class="c-header c-header--archive c-header--project-list">
                                                                                    <div class="c-mouse-vertical-carousel js-carousel u-media-wrapper">
                                                                                        <div class="carousel__header">
                                                                                            <h2>Our Rooms</h2>
                                                                                        </div>

                                                                                        <ul class="c-mouse-vertical-carousel__list js-carousel-list">
                                                                                            <?php $x = 0; ?>
                                                                                            @foreach($rooms as $room)
                                                                                                <li class="c-mouse-vertical-carousel__list-item js-carousel-list-item" data-item-id="{{ $x }}">
                                                                                                    <a href="https://isho.com/popup-rooms/{{$room->slug}}">
                                                                                                        <p class="c-mouse-vertical-carousel__eyebrow">from {{$room->price}}/night
                                                                                                        </p>
                                                                                                        <p class="c-mouse-vertical-carousel__title">
                                                                                                            {{$room->name}}
                                                                                                        </p>
                                                                                                    </a>
                                                                                                </li>
                                                                                                <?php $x += 1; ?>
                                                                                            @endforeach

                                                                                          {{--  <li class="c-mouse-vertical-carousel__list-item js-carousel-list-item" data-item-id="1">
                                                                                                <a href="deluxe-room.html">
                                                                                                    <p class="c-mouse-vertical-carousel__eyebrow">
                                                                                                        <!-- from $249/night -->
                                                                                                    </p>
                                                                                                    <p class="c-mouse-vertical-carousel__title">
                                                                                                        Williamsburg
                                                                                                    </p>
                                                                                                </a>
                                                                                            </li>--}}
                                                                                            {{--<li class="c-mouse-vertical-carousel__list-item js-carousel-list-item" data-item-id="2">
                                                                                                <a href="deluxe-room.html">
                                                                                                    <p class="c-mouse-vertical-carousel__eyebrow">
                                                                                                        <!-- from $299/night -->
                                                                                                    </p>
                                                                                                    <p class="c-mouse-vertical-carousel__title">
                                                                                                        Ratargul
                                                                                                    </p>
                                                                                                </a>
                                                                                            </li>--}}
                                                                                         {{--   <li class="c-mouse-vertical-carousel__list-item js-carousel-list-item" data-item-id="3">
                                                                                                <a href="deluxe-room.html">
                                                                                                    <p class="c-mouse-vertical-carousel__eyebrow">
                                                                                                        <!-- from $399/night -->
                                                                                                    </p>
                                                                                                    <p class="c-mouse-vertical-carousel__title">
                                                                                                    Navigli
                                                                                                    </p>
                                                                                                </a>
                                                                                            </li>--}}
                                                                                        </ul>
                                                                                        <i class="c-mouse-vertical-carousel__bg-img js-carousel-bg-img" style="background-image: url(https://isho.com/storage/media/pop-up/room1.jpg);"
                                                                                        ></i>
                                                                                        <i class="c-mouse-vertical-carousel__bg-img js-carousel-bg-img" style="background-image: url(https://isho.com/storage/media/pop-up/room2.jpg);"></i>
                                                                                        <i class="c-mouse-vertical-carousel__bg-img js-carousel-bg-img" style="background-image: url(https://isho.com/storage/media/pop-up/room3.jpg);"></i>
                                                                                        <i class="c-mouse-vertical-carousel__bg-img js-carousel-bg-img" style="background-image: url(https://isho.com/storage/media/pop-up/room4.jpg);"></i>
                                                                                        <i class="c-gradient-overlay"></i>
                                                                                    </div>
                                                                                </header>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-521055f elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="521055f"
                                                data-element_type="section"
                                                data-settings='{"hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}'>
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-77e3d6e"
                                                            data-id="77e3d6e"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}' >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-a807720 elementor-widget elementor-widget-image"
                                                                        data-id="a807720"
                                                                        data-element_type="widget"
                                                                        data-settings='{"hoteller_image_is_animation":"false","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="image.default" >
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <img
                                                                                    width="960"
                                                                                    height="1440"
                                                                                    src="https://isho.com/storage/media/pop-up/story2.jpg"
                                                                                    class="attachment-full size-full"
                                                                                    alt=""
                                                                                    loading="lazy"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-dde9ff1"
                                                            data-id="dde9ff1"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}' >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <!-- <div
                                                                        class="elementor-element elementor-element-7ecb65d elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="7ecb65d"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default" >
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">Luxury &amp; Comfort</h2>
                                                                        </div>
                                                                    </div> -->
                                                                    <div
                                                                        class="elementor-element elementor-element-8deffd0 elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="8deffd0"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">Cocoons of Luxury</h2>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-cdd163e elementor-invisible elementor-widget elementor-widget-text-editor"
                                                                        data-id="cdd163e"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":200,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                                <p>Built using natural and sustainable raw materials, the hotel features 4 double-occupancy rooms that offer unbridled comfort. Featuring pristine white wooden exteriors and finished with bamboo-thatched roofing in the balconies, each room showcases a different theme. </p>
                                                                                <p>We have the Melrose, Ratargul, Williamsburg and Navigli- each one with a distinctive look and feel, for a living experience that’s truly one of its kind.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-48c0d38 elementor-align-left elementor-mobile-align-center elementor-invisible elementor-widget elementor-widget-button"
                                                                        data-id="48c0d38"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":400,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="button.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-button-wrapper">
                                                                                <a href="/popup-rooms/deluxe" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                                                                    <span class="elementor-button-content-wrapper">
                                                                                        <span class="elementor-button-text">View more</span>
                                                                                    </span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-30563d8 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                                                data-id="30563d8"
                                                data-element_type="section"
                                                data-settings='{"stretch_section":"section-stretched","background_background":"classic","hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}' >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-068a6eb"
                                                            data-id="068a6eb"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'>
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <!-- <div
                                                                        class="elementor-element elementor-element-57ad2cc elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="57ad2cc"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default"
                                                                    >
                                                                        <div class="elementor-widget-container">
                                                                            <span class="elementor-heading-title elementor-size-default">Nearby Places</span>
                                                                        </div>
                                                                    </div> -->
                                                                    <div
                                                                        class="elementor-element elementor-element-4d21ea3 elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="4d21ea3"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":200,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default" >
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">
                                                                                A Gourmet Celebration
                                                                            </h2>
                                                                        </div>
                                                                    </div>
                                                                    <!-- <div
                                                                        class="elementor-element elementor-element-bbbf394 elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="bbbf394"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":400,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default"
                                                                    >
                                                                        <div class="elementor-widget-container">
                                                                            <span class="elementor-heading-title elementor-size-default">Correspondingly during this time, there are a lot of parties, and happenings going on</span>
                                                                        </div>
                                                                    </div> -->
                                                                    <div
                                                                        class="elementor-element elementor-element-0358bbe elementor-invisible elementor-widget elementor-widget-text-editor"
                                                                        data-id="0358bbe"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":600,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container elementor-para">
                                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                                <p class="p1">Our outdoor restaurant has been designed for an intimate setting and serves you the best of local cuisine prepared from fresh ingredients.
                                                                                    Savour the simple flavours of the region, elevated with our chef’s Midas touch.
                                                                                    Beautifully plated, so every dish served is a work of art.
                                                                                    Enjoy your meal in a relaxing ambience, amid the lilting sounds of the waves .</p>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-a622a86 elementor-invisible elementor-widget elementor-widget-hoteller-slider-synchronized-carousel"
                                                                        data-id="a622a86"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeIn","_animation_delay":800,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="hoteller-slider-synchronized-carousel.default" >
                                                                        <div class="elementor-widget-container">
                                                                        <!--  -->
                                                                            <div
                                                                                id="tg_synchronized_carousel_slider_a622a86"
                                                                                data-pagination="tg_synchronized_carousel_pagination_a622a86"
                                                                                class="tg_synchronized_carousel_slider_wrapper sliders-container"
                                                                                data-countslide="3"
                                                                                data-slidetitles='["Beautiful Highest Alph Mountain","Skiing in the forest","Experience mountain train express"]'
                                                                                data-slideimages='["https://www.isho.com/storage/media/g5MckLLLTObF5guB4ugGQR2CpvOvp9WuA6nJEzW0.jpeg","https://www.isho.com/storage/media/t27R1l9HrQMYGIDkgJvD18fFDzAMnmuurNIldT1U.jpeg","https://www.isho.com/storage/media/NiZadgyQZ4qI83s5nrUbNu4ytLzzpa08HjtbEKKQ.jpeg"]'
                                                                                data-slidebuttontitles='["","",""]'
                                                                                data-slidebuttonurls='["","",""]'>
                                                                                <ul id="tg_synchronized_carousel_pagination_a622a86" class="tg_synchronized_carousel_pagination">
                                                                                    <li class="pagination__item"><a class="pagination__button"></a></li>
                                                                                    <li class="pagination__item"><a class="pagination__button"></a></li>
                                                                                    <li class="pagination__item"><a class="pagination__button"></a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <br class="clear" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-bfd23ce elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="bfd23ce"
                                                data-element_type="section"
                                                data-settings='{"hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}' >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-75eff42"
                                                            data-id="75eff42"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}' >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-e4d6ae5 elementor-invisible elementor-widget elementor-widget-image"
                                                                        data-id="e4d6ae5"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeIn","hoteller_image_is_animation":"false","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="image.default" >
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <img
                                                                                    width="1947"
                                                                                    height="555"
                                                                                    src="https://www.isho.com/storage/media/2R3nos5QNtLtDIzC9AiAbTGXArf3HOKimBbnpczt.jpeg"
                                                                                    class="attachment-full size-full"
                                                                                    alt=""
                                                                                    loading="lazy" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-e0b2fea elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="e0b2fea"
                                                data-element_type="section"
                                                data-settings='{"hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}' >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-b7f2abc"
                                                            data-id="b7f2abc"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}' >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <!-- <div
                                                                        class="elementor-element elementor-element-8b6f69b elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="8b6f69b"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default"
                                                                    >
                                                                        <div class="elementor-widget-container">
                                                                            <span class="elementor-heading-title elementor-size-default">Urban & Unique</span>
                                                                        </div>
                                                                    </div> -->
                                                                    <div
                                                                        class="elementor-element elementor-element-501d6ad elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="501d6ad"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":200,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default" >
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">
                                                                                Discover Inspired Living
                                                                            </h2>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-3b6fc07 elementor-invisible elementor-widget elementor-widget-text-editor"
                                                                        data-id="3b6fc07"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","_animation_delay":400,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container elementor-para">
                                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                                <p class="p1">Situated on the palm-fringed shore, the pop-up features a wooden-decked lounge area,
                                                                                    an outdoor restaurant, beachside barbeque, a quaint gift shop and a concierge to assist you with all your needs.


                                                                                </p>
                                                                                <p class="p1">
                                                                                    Each room is fully equipped with a comfortable bed, a spacious cabinet, a bedside table and an en-suite bathroom. The room capitalizes on the natural light entering it and showcases floor lamps and sconce lights in its interiors. The fresh sea breeze provides natural air-conditioning and ample ventilation.
                                                                                </p>
                                                                                <p class="p1">
                                                                                    Impeccable room service, free Wi-Fi and self-parking are also provided at the property.
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section style="background-attachment: fixed;" class="elementor-section elementor-top-section elementor-element elementor-element-5b9bf0d elementor-section-stretched elementor-section-height-min-height elementor-section-items-bottom elementor-section-boxed elementor-section-height-default"
                                                data-id="5b9bf0d"
                                                data-element_type="section"
                                                data-settings='{"stretch_section":"section-stretched","background_background":"classic","hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}' >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6f59c36"
                                                            data-id="6f59c36"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'>
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-46767e3 elementor-widget elementor-widget-heading"
                                                                        data-id="46767e3"
                                                                        data-element_type="widget"
                                                                        data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <!-- <span class="elementor-heading-title elementor-size-default">
                                                                                Each of our properties are uniquely positioned but are connected through a trail of peerless service
                                                                            </span> -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-f7996f9 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="f7996f9"
                                                data-element_type="section"
                                                data-settings='{"hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}' >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f72cecc"
                                                            data-id="f72cecc"
                                                            data-element_type="column"
                                                            data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}' >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-2bae295 elementor-invisible elementor-widget elementor-widget-heading"
                                                                        data-id="2bae295"
                                                                        data-element_type="widget"
                                                                        data-settings='{"_animation":"fadeInUp","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="heading.default" >
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">Check out this space for weekly updates</h2>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-ff18320 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="ff18320"
                                                data-element_type="section"
                                                data-settings='{"hoteller_ext_is_background_parallax":"false","hoteller_ext_is_background_on_scroll":"false"}' >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div
                                                            class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-db76093 elementor-invisible"
                                                            data-id="db76093"
                                                            data-element_type="column"
                                                            data-settings='{"animation":"fadeInLeft","hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}' >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-dd593ac elementor-widget elementor-widget-hoteller-flip-box"
                                                                        data-id="dd593ac"
                                                                        data-element_type="widget"
                                                                        data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="hoteller-flip-box.default" >
                                                                        <div class="elementor-widget-container">
                                                                            <div class="tg_flip_box_wrapper square-flip">
                                                                                <div class="square" data-image="https://isho.com/storage/media/pop-up/offers1.jpg">
                                                                                    <div class="square-container">
                                                                                        <h2>Places Nearby & Things to Do</h2>
                                                                                    </div>
                                                                                    <div class="flip-overlay"></div>
                                                                                </div>
                                                                                <div class="square2" data-image="https://isho.com/storage/media/pop-up/offers1.jpg">
                                                                                    <div class="square-container2">
                                                                                        <div class="align-center"></div>
                                                                                        <h2>Breakfast included when booking the Junior Suite</h2>
                                                                                        <a href="index.html" class="boxshadow button">View Offer</a>
                                                                                    </div>
                                                                                    <div class="flip-overlay"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-ced3245 elementor-invisible"
                                                            data-id="ced3245"
                                                            data-element_type="column"
                                                            data-settings='{"animation":"fadeInLeft","animation_delay":200,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}' >
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-b6c7c60 elementor-widget elementor-widget-hoteller-flip-box"
                                                                        data-id="b6c7c60"
                                                                        data-element_type="widget"
                                                                        data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="hoteller-flip-box.default" >
                                                                        <div class="elementor-widget-container">
                                                                            <div class="tg_flip_box_wrapper square-flip">
                                                                                <div class="square" data-image="https://isho.com/storage/media/pop-up/offers2.jpg">
                                                                                    <div class="square-container">
                                                                                        <h2>Fun & Adventure on the Beach</h2>
                                                                                    </div>
                                                                                    <div class="flip-overlay"></div>
                                                                                </div>
                                                                                <div class="square2" data-image="https://isho.com/storage/media/pop-up/offers2.jpg">
                                                                                    <div class="square-container2">
                                                                                        <div class="align-center"></div>
                                                                                        <h2>Benefit from a 10% discount, reservations with a minimum of 3 days in advance</h2>
                                                                                        <a href="index.html" class="boxshadow button">View Offer</a>
                                                                                    </div>
                                                                                    <div class="flip-overlay"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-747f6e2 elementor-invisible"
                                                            data-id="747f6e2"
                                                            data-element_type="column"
                                                            data-settings='{"animation":"fadeInLeft","animation_delay":400,"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'>
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                        class="elementor-element elementor-element-9ae5967 elementor-widget elementor-widget-hoteller-flip-box"
                                                                        data-id="9ae5967"
                                                                        data-element_type="widget"
                                                                        data-settings='{"hoteller_ext_is_scrollme":"false","hoteller_ext_is_smoove":"false","hoteller_ext_is_parallax_mouse":"false","hoteller_ext_is_infinite":"false","hoteller_ext_is_fadeout_animation":"false"}'
                                                                        data-widget_type="hoteller-flip-box.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="tg_flip_box_wrapper square-flip">
                                                                                <div class="square" data-image="https://isho.com/storage/media/pop-up/offers3.jpg">
                                                                                    <div class="square-container">
                                                                                        <h2>Discounts for Couples</h2>
                                                                                    </div>
                                                                                    <div class="flip-overlay"></div>
                                                                                </div>
                                                                                <div class="square2" data-image="https://isho.com/storage/media/pop-up/offers3.jpg">
                                                                                    <div class="square-container2">
                                                                                        <div class="align-center"></div>
                                                                                        <h2>Benefit from a 15% discount, making your reservations with a minimum of 15 days in advance</h2>
                                                                                        <a href="index.html" class="boxshadow button">View Offer</a>
                                                                                    </div>
                                                                                    <div class="flip-overlay"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>

                                            <!-- weekly activities -->
                                            <section class="weekly_activities elementor-section elementor-top-section elementor-element elementor-element-f7996f9 elementor-section-boxed elementor-section-height-default elementor-section-height-default">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row_box">
                                                        <div class="elementor-widget-container">
                                                            <h2 class="elementor-heading-title elementor-size-default">Weekly Calendar</h2>
                                                        </div>
                                                        <div class="calendar-area">
                                                            <div class="daybox" id="popup-day-activity_sat">
                                                                <a onclick="popup_day('sat')">Saturday</a>
                                                            </div>
                                                            <div class="daybox" id="popup-day-activity_sun">
                                                                <a onclick="popup_day('sun')">Sunday</a>
                                                            </div>
                                                            <div class="daybox" id="popup-day-activity_mon">
                                                                <a onclick="popup_day('mon')">Monday</a>
                                                            </div>
                                                            <div class="daybox" id="popup-day-activity_tue">
                                                                <a onclick="popup_day('tue')">Tuesday</a>
                                                            </div>
                                                            <div class="daybox" id="popup-day-activity_wed">
                                                                <a onclick="popup_day('wed')">Wednesday</a>
                                                            </div>
                                                            <div class="daybox" id="popup-day-activity_thu">
                                                                <a onclick="popup_day('thu')">Thursday</a>
                                                            </div>
                                                            <div class="daybox" id="popup-day-activity_fri">
                                                                <a onclick="popup_day('fri')">Friday</a>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </section>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End main content -->
                    </div>
                    <br class="clear" />
                </div>
            </div>

            <!-- Start Footer Content-->

        </div>

        <div class="day-activity_show" id="popup-day-activity_sat1">
            <h3 class="title">Saturday Activities
                <br><span>Opening hours : 8.00am to 11.00pm</span>
            </h3>
            <span class="popup-close">x</span>
            <div class="blocks-wrap">
                <div class="block">
                    <div class="">
                        <p><strong>Yoga on the beach at : </strong> 8.00am to 3.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>Workshop with bla bla at : </strong> 4pm to 11.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>The Bar-B-Q :</strong> 5.00pm to 11.00pm</p>
                    </div>
                </div>
            </div>
            <h6 class="bottom_text">End of the day</span></h6>
        </div>
        <div class="day-activity_show" id="popup-day-activity_sun1">
            <h3 class="title">Sunday Activities
                <br><span>Opening hours : 8.00am to 11.00pm</span>
            </h3>
            <span class="popup-close">x</span>
            <div class="blocks-wrap">
                <div class="block">
                    <div class="">
                        <p><strong>Yoga on the beach at : </strong> 8.00am to 3.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>Workshop with bla bla at : </strong> 4pm to 11.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>The Bar-B-Q :</strong> 5.00pm to 11.00pm</p>
                    </div>
                </div>
            </div>
            <h6 class="bottom_text">End of the day</span></h6>
        </div>
        <div class="day-activity_show" id="popup-day-activity_mon1">
            <h3 class="title">Monday Activities
                <br><span>Opening hours : 8.00am to 11.00pm</span>
            </h3>
            <span class="popup-close">x</span>
            <div class="blocks-wrap">
                <div class="block">
                    <div class="">
                        <p><strong>Yoga on the beach at : </strong> 8.00am to 3.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>Workshop with bla bla at : </strong> 4pm to 11.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>The Bar-B-Q :</strong> 5.00pm to 11.00pm</p>
                    </div>
                </div>
            </div>
            <h6 class="bottom_text">End of the day</span></h6>
        </div>
        <div class="day-activity_show" id="popup-day-activity_tue1">
            <h3 class="title">Tuesday Activities
                <br><span>Opening hours : 8.00am to 11.00pm</span>
            </h3>
            <span class="popup-close">x</span>
            <div class="blocks-wrap">
                <div class="block">
                    <div class="">
                        <p><strong>Yoga on the beach at : </strong> 8.00am to 3.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>Workshop with bla bla at : </strong> 4pm to 11.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>The Bar-B-Q :</strong> 5.00pm to 11.00pm</p>
                    </div>
                </div>
            </div>
            <h6 class="bottom_text">End of the day</span></h6>
        </div>
        <div class="day-activity_show" id="popup-day-activity_wed1">
            <h3 class="title">Wednesday Activities
                <br><span>Opening hours : 8.00am to 11.00pm</span>
            </h3>
            <span class="popup-close">x</span>
            <div class="blocks-wrap">
                <div class="block">
                    <div class="">
                        <p><strong>Yoga on the beach at : </strong> 8.00am to 3.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>Workshop with bla bla at : </strong> 4pm to 11.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>The Bar-B-Q :</strong> 5.00pm to 11.00pm</p>
                    </div>
                </div>
            </div>
            <h6 class="bottom_text">End of the day</span></h6>
        </div>
        <div class="day-activity_show" id="popup-day-activity_thu1">
            <h3 class="title">Thursday Activities
                <br><span>Opening hours : 8.00am to 11.00pm</span>
            </h3>
            <span class="popup-close">x</span>
            <div class="blocks-wrap">
                <div class="block">
                    <div class="">
                        <p><strong>Yoga on the beach at : </strong> 8.00am to 3.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>Workshop with bla bla at : </strong> 4pm to 11.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>The Bar-B-Q :</strong> 5.00pm to 11.00pm</p>
                    </div>
                </div>
            </div>
            <h6 class="bottom_text">End of the day</span></h6>
        </div>
        <div class="day-activity_show" id="popup-day-activity_fri1">
            <h3 class="title">Friday Activities
                <br><span>Opening hours : 8.00am to 11.00pm</span>
            </h3>
            <span class="popup-close">x</span>
            <div class="blocks-wrap">
                <div class="block">
                    <div class="">
                        <p><strong>Yoga on the beach at : </strong> 8.00am to 3.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>Workshop with bla bla at : </strong> 4pm to 11.00pm</p>
                    </div>
                </div>
                <div class="block">
                    <div class="">
                        <p><strong>The Bar-B-Q :</strong> 5.00pm to 11.00pm</p>
                    </div>
                </div>
            </div>
            <h6 class="bottom_text">End of the day</span></h6>
        </div>

@endsection


@section('page_script')
    <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/jquery.js?ver=1.12.4-wp" id="jquery-core-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/ui/core.min.js" id="jquery-ui-core-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/ui/widget.min.js" id="jquery-ui-widget-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/ui/mouse.min.js" id="jquery-ui-mouse-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/ui/resizable.min.js" id="jquery-ui-resizable-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/ui/draggable.min.js" id="jquery-ui-draggable-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/ui/button.min.js" id="jquery-ui-button-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/ui/position.min.js" id="jquery-ui-position-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/ui/dialog.min.js" id="jquery-ui-dialog-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/imagesloaded.min.js?ver=4.1.4" id="imagesloaded-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/masonry.min.js?ver=4.2.2" id="masonry-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/jquery.lazy.js" id="lazy-js"></script>
        <script>
            //  for video auto play
            // document.getElementById('vid').play();
        </script>
        <script type="text/javascript" id="lazy-js-after">
            jQuery(function ($) {
                jQuery("img.lazy").each(function () {
                    var currentImg = jQuery(this);

                    jQuery(this).Lazy({
                        onFinishedAll: function () {
                            currentImg.parent("div.post_img_hover").removeClass("lazy");
                            currentImg.parent(".tg_gallery_lightbox").parent("div.gallery_grid_item").removeClass("lazy");
                            currentImg.parent("div.gallery_grid_item").removeClass("lazy");
                        },
                    });
                });
            });
        </script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/modulobox.js" id="modulobox-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/jquery.parallax-scroll.js" id="parallax-scroll-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/jquery.smoove.js" id="smoove-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/parallax.js" id="parallax-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/jquery.blast.js" id="blast-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/jquery.visible.js" id="visible-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/jarallax.js" id="jarallax-js"></script>

        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/ui/effect.min.js" id="jquery-effects-core-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/waypoints.min.js" id="waypoints-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/tilt.jquery.js" id="tilt-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/jquery.stellar.min.js" id="stellar-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/custom_plugins.js" id="hoteller-custom-plugins-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/custom.js" id="hoteller-custom-script-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/jquery.sticky-kit.min.js" id="sticky-kit-js"></script>
        <script type="text/javascript" id="sticky-kit-js-after">
            jQuery(function ($) {
                jQuery("#page_content_wrapper .sidebar_wrapper").stick_in_parent({ offset_top: 100, recalc_every: 1 });

                if (jQuery(window).width() < 768 || is_touch_device()) {
                    jQuery("#page_content_wrapper .sidebar_wrapper").trigger("sticky_kit:detach");
                }
            });
        </script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/loftloader/assets/js/loftloader.min.js?ver=2020081301" id="loftloader-lite-front-main-js"></script>
        <script type="text/javascript" id="hoteller-elementor-js-extra">
            /* <![CDATA[ */
            var tgAjax = { ajaxurl: "#", ajax_nonce: "198d31930d" };
            /* ]]> */
        </script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/hoteller-elementor.js" id="hoteller-elementor-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/three.min.js" id="three-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/TweenMax.js" id="tweenmax-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/momentum-slider.js?ver=5.5.3" id="momentum-slider-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/hover.js" id="hover-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/hoteller-elementor/assets/js/flickity.pkgd.js" id="flickity-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/jquery.cookie.js" id="hoteller-jquery-cookie-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/jquery.tooltipster.min.js" id="tooltipster-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/demo.js" id="hoteller-demo-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/js/frontend-modules.min.js" id="elementor-frontend-modules-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.8.1" id="elementor-dialog-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2" id="elementor-waypoints-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6" id="swiper-js"></script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/lib/share-link/share-link.min.js" id="share-link-js"></script>
        <script type="text/javascript" id="elementor-frontend-js-before">
            var elementorFrontendConfig = {
                environmentMode: { edit: false, wpPreview: false },
                i18n: {
                    shareOnFacebook: "Share on Facebook",
                    shareOnTwitter: "Share on Twitter",
                    pinIt: "Pin it",
                    download: "Download",
                    downloadImage: "Download image",
                    fullscreen: "Fullscreen",
                    zoom: "Zoom",
                    share: "Share",
                    playVideo: "Play Video",
                    previous: "Previous",
                    next: "Next",
                    close: "Close",
                },
                is_rtl: false,
                breakpoints: { xs: 0, sm: 480, md: 768, lg: 1025, xl: 1440, xxl: 1600 },
                version: "3.0.4",
                is_static: false,
                legacyMode: { elementWrappers: true },
                urls: { assets: "js//plugins//elementor//assets//" },
                settings: { page: [], editorPreferences: [] },
                kit: {
                    global_image_lightbox: "yes",
                    lightbox_enable_counter: "yes",
                    lightbox_enable_fullscreen: "yes",
                    lightbox_enable_zoom: "yes",
                    lightbox_enable_share: "yes",
                    lightbox_title_src: "title",
                    lightbox_description_src: "description",
                },
                post: { id: 662, title: "Hoteller%20%7C%20Luxury%20Hotel%20WordPress%20Theme%20%E2%80%93%20Just%20another%20WordPress%20site", excerpt: "", featuredImage: false },
            };

            function popup_day(val){
                console.log(val);
                var day_id = 'popup-day-activity_'+val;
                var day_show = 'popup-day-activity_'+val+1;
                day_id = document.getElementById(day_id);
                day_show = document.getElementById(day_show);
                $('.day-activity_show').fadeOut();
                if(day_show){
                    setTimeout(function () {
                        $(day_show).fadeIn();
                        $(this).parentNode.classList.add("active");
                    }, 20);
                }
            }

            $('.day-activity_show .popup-close').on('click', function () {
                $('.day-activity_show').fadeOut();
            });

            $(document).mouseup(function(e) {
                var container = $(".day-activity_show");
                if (!container.is(e.target) && container.has(e.target).length === 0)
                {
                    $('.day-activity_show').fadeOut();
                }
            });

        </script>
        <script type="text/javascript" src="https://isho.com/themes/storefront/isho/popup-hotel/js/plugins/elementor/assets/js/frontend.min.js" id="elementor-frontend-js"></script>
@endsection
